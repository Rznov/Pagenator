//
//  PGNPageViewController.h
//  Pagenator
//
//  Created by Артем Розанов on 10.11.2020.
//  Copyright © 2020 Артем Розанов. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PGNPageViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (copy, nonatomic) NSMutableArray *cats;

@end

NS_ASSUME_NONNULL_END
