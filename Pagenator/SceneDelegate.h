//
//  SceneDelegate.h
//  Pagenator
//
//  Created by Артем Розанов on 30.10.2020.
//  Copyright © 2020 Артем Розанов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

